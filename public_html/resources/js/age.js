/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getSignificationOfAge() {
    var gender = getGender();
    var age = document.getElementById("age").value;
    if (isNaN(age) || isEmptyOrUndefined(age) || (age < 0)) {
        alert('Veuillez saisir un nombre (positif) s\'il vous plaît !');
    }
    age = parseInt(age);

    var res = "";

    switch (gender) {
        case 'f':
            if (age < 0) {
                alert('Veuillez saisir un nombre positif');
            } else if (age === 0 || age === 1) {
                res = "Vous êtes un bébé.";
            } else if ((1 < age) && (age < 7)) {
                res = "Vous êtes une jeune fille.";
            } else if ((6 < age) && (age < 12)) {
                res = "Vous êtes une enfant qui a atteint l'âge de raison.";
            } else if ((11 < age) && (age < 18)) {
                res = "Vous êtes une adolescente.";
            } else {
                res = "Vous êtes & vous serez toujours une jeune adulte. C'est dans la tête tout ça !";
            }
            break;
        case 'm':
            if (age < 0) {
                alert('Veuillez saisir un nombre positif');
            } else if ((age === 0) || (age === 1)) {
                res = "Vous êtes un bébé.";
            } else if ((1 < age) && (age < 7)) {
                res = "Vous êtes un jeune enfant.";
            } else if ((6 < age) && (age < 12)) {
                res = "Vous êtes un enfant qui a atteint l'âge de raison.";
            } else if ((11 < age) && (age < 18)) {
                res = "Vous êtes un adolescent.";
            } else {
                res = "Vous êtes & vous serez toujours un jeune adulte. C'est dans la tête tout ça !";
            }
            break;
        default :
            if (age < 0) {
                alert('Veuillez saisir un nombre positif');
            } else if ((age === 0) || (age === 1)) {
                res = "Vous êtes un bébé.";
            } else if ((1 < age) && (age < 7)) {
                res = "Vous êtes un(e) jeune enfant (fille).";
            } else if ((6 < age) && (age < 12)) {
                res = "Vous êtes un(e) enfant qui a atteint l'âge de raison.";
            } else if ((11 < age) && (age < 18)) {
                res = "Vous êtes un(e) adolescent(e).";
            } else {
                res = "Vous êtes & vous serez toujours un(e) jeune adulte. C'est dans la tête tout ça !";
            }
            break;
    }

    document.getElementById("result").innerHTML = res;
}

function getGender() {
    var radios = document.getElementsByName('gender');
    var length = radios.length;
    for (var i = 0; i < length; i++)
    {
        if (radios[i].checked)
        {
            return radios[i].value;
            break;
        }
    }
}

function isEmptyOrUndefined(age) {
    return ((age === undefined) || (age.length === 0));
}

